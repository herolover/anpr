#include <iostream>
#include <string>
#include <unordered_map>
#include <functional>
#include <algorithm>
#include <utility>

#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include "ANPR.h"
#include "place_number_recognizer.h"
#include "help_opencv.h"

cv::Mat load_and_display_image(const std::string &filename, double small_image_ratio=0.3)
{
  std::cout << filename << std::endl;

  cv::Mat image = cv::imread(filename);

  cv::Mat small_image;
  cv::resize(image, small_image, cv::Size(), small_image_ratio, small_image_ratio);
  cv::imshow("image", small_image);

  return image;
}

int main(int argc, char *argv[])
{
  namespace fs = boost::filesystem;
  std::vector<fs::path> files;
  fs::path path(argv[1]);
  for (fs::directory_iterator file_it(path); file_it != fs::directory_iterator(); ++file_it)
    if (file_it->path().extension() == ".jpg")
      files.push_back(file_it->path());
  std::sort(files.begin(), files.end());

  cv::namedWindow("image");

  cv::Mat image;
  auto file_it = files.begin();
  image = load_and_display_image(file_it->generic_string());

  enum KEY
  {
    ESCAPE_KEY = 27,
    LEFT_KEY = 65361,
    RIGHT_KEY = 65363,
    SPACE_KEY = 32
  };

  KEY key;
  while ((key = (KEY)cv::waitKey(1)) != ESCAPE_KEY)
  {
    if (key == LEFT_KEY)
    {
      if (file_it - 1 >= files.begin())
        image = load_and_display_image((--file_it)->generic_string());
    }
    else if (key == RIGHT_KEY)
    {
      if (file_it + 1 < files.end())
        image = load_and_display_image((++file_it)->generic_string());
    }
    else if (key == SPACE_KEY)
    {
      auto number_plate = ANPR::recognize_number_plate(image);
//      std::cout << number_plate.first << std::endl;
      std::cout << recognize_place_number(image, number_plate.second).first << std::endl;
    }
  }

  return 0;
}
