#include "place_number_recognizer.h"

#include "help_opencv.h"
#include "help_alg.h"

#include <algorithm>
#include <set>

#include <tesseract/baseapi.h>

struct Contour
{
  std::vector<cv::Point> points;
  std::vector<cv::Point> corners;
  cv::Rect aabb;
};

std::vector<cv::Point> get_neighbours(const cv::Mat &contour_image, const cv::Point &point)
{
  const std::vector<cv::Point> deltas = {cv::Point( 0, -1),
                                         cv::Point( 1,  0),
                                         cv::Point( 0,  1),
                                         cv::Point(-1,  0)};
  std::vector<cv::Point> neighbours;
  for (auto &delta: deltas)
  {
    cv::Point neighbor = point + delta;
    if (neighbor.x >= 0 && neighbor.x < contour_image.cols &&
        neighbor.y >= 0 && neighbor.y < contour_image.rows &&
        contour_image.at<unsigned char>(neighbor) != 0)
      neighbours.push_back(neighbor);
  }

  return neighbours;
}

bool find_closed_contour(cv::Mat &contour_image, std::vector<cv::Point> &contour, const cv::Point &start_point)
{
  // select the end point
  cv::Point end_point;
  {
    auto neighbours = get_neighbours(contour_image, start_point);
    if (neighbours.size() == 2)
      end_point = neighbours[1];
    else
      return false;
  }

  cv::Point current_point = start_point;
  while (true)
  {
    contour.push_back(current_point);
    contour_image.at<unsigned char>(current_point) = 0;

    if (current_point == end_point)
      return true;

    auto neighbours = get_neighbours(contour_image, current_point);
    if (neighbours.size() == 1 ||
        neighbours.size() == 2 && current_point == start_point)
      current_point = neighbours[0];
    else
      return false;
  }
}

std::vector<cv::Point> find_corners(const std::vector<cv::Point> &contour, std::size_t number_of_corners,
                                    std::size_t side_length=20)
{
  std::vector<std::pair<std::size_t, double>> estimates;
  for (std::size_t i = 0; i < contour.size(); ++i)
  {
    std::size_t left_point_index = i < side_length ? contour.size() - ((side_length - i) % contour.size()) : i - side_length;
    std::size_t right_point_index = i + side_length >= contour.size() ? (side_length + i) % contour.size() : i + side_length;

    cv::Point2d left_side = contour[left_point_index] - contour[i];
    cv::Point2d right_side = contour[right_point_index] - contour[i];

    left_side *= 1.0 / cv::norm(left_side);
    right_side *= 1.0 / cv::norm(right_side);

    estimates.push_back(std::make_pair(i, left_side.dot(right_side)));
  }

  std::sort(estimates.begin(), estimates.end(),
            [](const std::pair<std::size_t, double> &a, const std::pair<std::size_t, double> &b)
  {
    return a.second > b.second;
  });

  std::set<std::size_t> corner_indexes;
  for (auto &estimate: estimates)
  {
    bool to_add = true;
    for (auto &corner_index: corner_indexes)
      if (cv::norm(contour[corner_index] - contour[estimate.first]) < side_length)
      {
        to_add = false;
        break;
      }

    if (to_add)
    {
      corner_indexes.insert(estimate.first);
      if (corner_indexes.size() == number_of_corners)
        break;
    }
  }

  std::vector<cv::Point> corners;
  for (auto &corner_index: corner_indexes)
    corners.push_back(contour[corner_index]);

  return corners;
}

bool is_quad(const Contour &contour)
{
  cv::Mat image = cv::Mat::zeros(contour.aabb.size(), CV_8UC1);
  draw_area(image, contour.points, 255, contour.aabb.tl());

  for (std::size_t i = 0; i < 4; ++i)
    cv::line(image, contour.corners[i] - contour.aabb.tl(), contour.corners[(i + 1) % 4] - contour.aabb.tl(), 50, 5);

//  cv::imshow("quad", image);

  unsigned white_pixel_counter = 0;
  for (int y = 0; y < image.rows; ++y)
    for (int x = 0; x < image.cols; ++x)
      if (image.at<unsigned char>(y, x) == 255)
        white_pixel_counter += 1;

  double k = white_pixel_counter / (double)contour.points.size();
//  std::cout << "Is rect: " << k << std::endl;

  return k < 0.04;
}

std::pair<std::string, cv::Rect> recognize_place_number(const cv::Mat &image,
                                                        const cv::Rect &number_plate_rect)
{
  cv::Mat denoised_image;
  cv::medianBlur(image, denoised_image, 3);

  cv::Mat lab_image;
  cv::cvtColor(denoised_image, lab_image, CV_BGR2Lab);

  std::vector<cv::Mat> channels;
  cv::split(lab_image, channels);

//  cv::imshow("channels", channels[1]);

  cv::Mat threshold_image;
  cv::adaptiveThreshold(channels[1], threshold_image, 255, cv::ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 151, -10);

//  cv::imshow("thresh", threshold_image);

  cv::Mat dilate_image;
  cv::dilate(threshold_image, dilate_image, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 9)));

  cv::Mat erode_image;
  cv::erode(dilate_image, erode_image, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)));

  cv::Mat contour_image = dilate_image - erode_image;

//  cv::imshow("contour_image", contour_image);
//  cv::imwrite("contour_image.jpg", contour_image);

  std::vector<Contour> contours;
  for (int x = 0; x < contour_image.cols; ++x)
    for (int y = 0; y < contour_image.rows; ++y)
      if (contour_image.at<unsigned char>(y, x) == 255)
      {
        Contour contour;
        find_closed_contour(contour_image, contour.points, cv::Point(x, y));
        if (contour.points.size() > 100)
        {
          contour.corners = find_corners(contour.points, 4);
          contour.aabb = cv::boundingRect(contour.points);
          contours.push_back(contour);
        }
      }

  contours.erase(std::remove_if(contours.begin(), contours.end(),
                                [&contours](const Contour &contour)
  {
    for (auto &another_contour: contours)
    {
      double k0 = (double)another_contour.aabb.width / another_contour.aabb.height;
      double k1 = (double)contour.aabb.width / contour.aabb.height;
      double k = k0 / k1;
      if (another_contour.aabb.contains(contour.aabb.tl()) &&
          another_contour.aabb.contains(contour.aabb.br()) &&
          k > 0.8 && k < 1.2 &&
          contour.corners.size() == 4 &&
          is_quad(contour))
        return false;
    }

    return true;
  }), contours.end());

//  for (std::size_t i = 0; i < contours.size(); ++i)
//  {
//    cv::Mat img = cv::Mat::zeros(contours[i].aabb.size(), CV_8UC1);
//    draw_area(img, contours[i].points, 255, contours[i].aabb.tl());
//    cv::imshow("contour" + std::to_string(i), img);
//  }

//  std::cout << "Contours: " << contours.size() << std::endl;

  if (contours.size() > 0)
  {
    Contour contour = *std::min_element(contours.begin(), contours.end(),
                                        [&image](const Contour &a, const Contour &b)
    {
      cv::Point image_center(image.cols / 2, image.rows / 2);
      cv::Point a_center(a.aabb.x + a.aabb.width / 2, a.aabb.y + a.aabb.height / 2);
      cv::Point b_center(b.aabb.x + b.aabb.width / 2, b.aabb.y + b.aabb.height / 2);

      return cv::norm(a_center - image_center) < cv::norm(b_center - image_center);
    });

    float mean_x = (contour.corners[0].x + contour.corners[1].x + contour.corners[2].x + contour.corners[3].x) / 4.0f;

    std::vector<cv::Point2f> left_corners;
    std::vector<cv::Point2f> right_corners;
    for (auto &corner: contour.corners)
      if (corner.x < mean_x)
        left_corners.push_back(corner);
      else
        right_corners.push_back(corner);
    std::sort(left_corners.begin(), left_corners.end(),
              [](const cv::Point2f &a, const cv::Point2f &b)
    {
      return a.y < b.y;
    });
    std::sort(right_corners.begin(), right_corners.end(),
              [](const cv::Point2f &a, const cv::Point2f &b)
    {
      return a.y < b.y;
    });

    int plate_size = 100;
    std::vector<cv::Point2f> original_points = {cv::Point2f(0, 0),
                                                cv::Point2f(plate_size, 0),
                                                cv::Point2f(plate_size, plate_size),
                                                cv::Point2f(0, plate_size)};
    std::vector<cv::Point2f> image_points = {left_corners[0],
                                             right_corners[0],
                                             right_corners[1],
                                             left_corners[1]};

    cv::Mat H = cv::findHomography(original_points, image_points).inv();

    cv::Mat place_number_image;
    cv::warpPerspective(denoised_image, place_number_image, H, cv::Size(plate_size, plate_size));

    cv::Mat gray_place_number_image;
    cv::cvtColor(place_number_image(cv::Rect(10, 10, plate_size - 20, plate_size - 20)), gray_place_number_image, CV_RGB2GRAY);

    cv::Mat blured_image;
    cv::GaussianBlur(gray_place_number_image, blured_image, cv::Size(31, 31), 0);

    cv::Mat grained_image = gray_place_number_image - blured_image + cv::Scalar(128);

    cv::Mat threshold_place_number_image;
    cv::threshold(grained_image, threshold_place_number_image, 120, 255, CV_THRESH_BINARY_INV);
//    cv::imshow("threshold_place_number_image", threshold_place_number_image);

    tesseract::TessBaseAPI tess_api;
    tess_api.Init("tessdata", "eng");
    tess_api.SetPageSegMode(tesseract::PSM_SINGLE_LINE);
    tess_api.SetVariable("tessedit_char_whitelist", "1234567890");

    tess_api.SetImage(threshold_place_number_image.ptr(),
                      threshold_place_number_image.size().width,
                      threshold_place_number_image.size().height,
                      threshold_place_number_image.elemSize(),
                      threshold_place_number_image.step1());
    char *text = tess_api.GetUTF8Text();
    std::string place_number = text;
    delete[] text;

    place_number.erase(std::remove_if(place_number.begin(),
                                      place_number.end(),
                                      [](int char_value)
    {
      return !isdigit(char_value);
    }), place_number.end());

    return std::make_pair(place_number, contour.aabb);
  }

  return std::make_pair("", cv::Rect());
}
